package cz.cvut.fel.via.cocktails.controller;

import cz.cvut.fel.via.cocktails.service.CocktailsService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/cocktails")
public class CocktailsController {

    private final CocktailsService cocktailsService;

    @GetMapping("/getByName")
    public ResponseEntity<Object[]> getByName(@RequestParam String searchedName){
        return ResponseEntity.ok(cocktailsService.getCocktailsByName(searchedName));
    }

    @GetMapping("/getByIngredient")
    public ResponseEntity<Object[]> getByIngredients(@RequestParam String searchedIngredients){
        return ResponseEntity.ok(cocktailsService.getCocktailsByIngredients(searchedIngredients));
    }
}
