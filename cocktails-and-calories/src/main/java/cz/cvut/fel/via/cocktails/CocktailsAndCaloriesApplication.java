package cz.cvut.fel.via.cocktails;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CocktailsAndCaloriesApplication {

	public static void main(String[] args) {
		SpringApplication.run(CocktailsAndCaloriesApplication.class, args);
	}

}
