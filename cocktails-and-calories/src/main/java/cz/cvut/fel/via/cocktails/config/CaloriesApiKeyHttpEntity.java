package cz.cvut.fel.via.cocktails.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

@Component
public class CaloriesApiKeyHttpEntity {

    @Value("${api.calories.key}")
    private String apiKey;

    @Value("${api.calories.headerHost}")
    private String headerHost;

    public HttpEntity<String> getHttpEntity(){
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-RapidAPI-Key", apiKey);
        headers.add("X-RapidAPI-Host", headerHost);
        return new HttpEntity<>(headers);
    }

}
