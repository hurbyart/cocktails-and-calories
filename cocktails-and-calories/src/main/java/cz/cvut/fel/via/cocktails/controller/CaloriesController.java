package cz.cvut.fel.via.cocktails.controller;

import cz.cvut.fel.via.cocktails.service.CaloriesService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("/calories")
public class CaloriesController {

    private final CaloriesService caloriesService;

    @GetMapping("/get")
    public ResponseEntity<Object> get(@RequestParam String name){
        return ResponseEntity.ok(caloriesService.get(name));
    }
}
