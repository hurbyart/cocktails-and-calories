package cz.cvut.fel.via.cocktails.service;

import cz.cvut.fel.via.cocktails.config.CocktailsApiKeyHttpEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@RequiredArgsConstructor
public class CocktailsService {

    private final RestTemplate restTemplate;
    private final CocktailsApiKeyHttpEntity cocktailsApiKeyHttpEntity;

    @Value("${api.cocktails.host}")
    private  String host;

    public Object[] getCocktailsByName(String name) {
        return restTemplate.exchange(
                host + "?name=" + name,
                HttpMethod.GET,
                cocktailsApiKeyHttpEntity.getHttpEntity(),
                Object[].class).getBody();
    }

    public Object[] getCocktailsByIngredients(String ingredients){
        return restTemplate.exchange(
                host + "?ingredients=" + ingredients,
                HttpMethod.GET,
                cocktailsApiKeyHttpEntity.getHttpEntity(),
                Object[].class).getBody();
    }
}
