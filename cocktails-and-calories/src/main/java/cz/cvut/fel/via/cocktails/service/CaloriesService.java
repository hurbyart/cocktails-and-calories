package cz.cvut.fel.via.cocktails.service;

import cz.cvut.fel.via.cocktails.config.CaloriesApiKeyHttpEntity;
import lombok.RequiredArgsConstructor;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

@Service
@RequiredArgsConstructor
public class CaloriesService {

    private final RestTemplate restTemplate;
    private final CaloriesApiKeyHttpEntity caloriesApiKeyHttpEntity;

    @Value("${api.calories.host}")
    private String host;

    public Object get(String name) {
        return restTemplate.exchange(
                host + "?query=" + name,
                HttpMethod.GET,
                caloriesApiKeyHttpEntity.getHttpEntity(),
                Object.class).getBody();
    }
}
