package cz.cvut.fel.via.cocktails.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.stereotype.Component;

@Component
public class CocktailsApiKeyHttpEntity {

    @Value("${api.cocktails.key}")
    private String apiKey;

    public HttpEntity<String> getHttpEntity(){
        HttpHeaders headers = new HttpHeaders();
        headers.add("X-Api-Key", apiKey);
        return new HttpEntity<>(headers);
    }
}
